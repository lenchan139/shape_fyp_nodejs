var express = require('express');
var router = express.Router();
// init mongodboose
var mongoose = require('mongoose');
var adminUtils = require('./modules/AdminUtils.js')
var parentUtils = require('./modules/ParentUtils.js')
mongoose.connect('mongodb://generealUser:123456@ds247347.mlab.com:47347/shape_fyp');
var applicantObj = parentUtils.getBaseApplicantObject(mongoose)
var adminUserObj = adminUtils.getBaseAdminUserObj(mongoose)
var optionsObj = adminUtils.getBaseOptionsObj(mongoose)
var bcrypt = require('bcrypt')
router.get('/', function(req, res, next) {
  adminUtils.userLoginChecker(req,res)
  res.render('admin/main', {
    title: 'admin main page',
    session: req.session
  })


});
router.get('/listAll', function(req, res, next){
  adminUtils.userLoginChecker(req,res)
  applicantObj.find({}, function(err,result){
    if(err){
      console.log(err)
    }else{
      console.log(result)
      res.render('admin/listAll', {
        title: 'admin main page',
        session: req.session,
        json:result
      })

    }
  })
})

router.get('/detail/:id', function(req,res, next){
  adminUtils.userLoginChecker(req,res)
  var id = req.params.id
  console.log(id)
  var query = applicantObj.findOne({"_id":id})
  query.exec(function(err,result){
    if(err){
      console.log(err)
    }else if(result){
      console.log(result)
      res.render('admin/detail', {
        title: 'admin main page',
        session: req.session,
        json:result
      })
    }
  })
});

router.get('/settings', function(req, res, next) {
  adminUtils.userLoginChecker(req,res)
  adminUserObj.find(function(err, result) {
      if(err){
        next(err)
      }else{
        console.log( result)
      res.render('admin/settings', {
        title: 'settings',
        session: req.session,
        adminUsers: result
      })
      }
    })


});

router.get('/search', function(req, res, next){
  adminUtils.userLoginChecker(req,res)
  applicantObj.find({}, function(err,result){

      if(err){
        next(err)
      }else{
        console.log(result)
        res.render('admin/search',{
          title: 'search result',
          session: req.session,
          respondObj: result
        })
      }
  })
})

router.post('/search', function(req, res, next){
  adminUtils.userLoginChecker(req,res)
  var ename = req.param('ename',null)
  var cname = req.param('cname',null)
  var nation = req.param('nation', null)
  var placeOfBirth = req.param('placeOfBirth', null)
  var religion = req.param('religion', null)
  var religionDeno = req.param('religionDeno', null)
  var address = req.param('address', null)
  var parentOcc = req.param('parentOcc', null)

  var respondObj =   null
  if(ename || cname || nation || placeOfBirth || religion || religionDeno || address ||parentOcc){
    respondObj = applicantObj.find()
    if(ename){
        respondObj = respondObj.or([{"applicantInfo.eFirstName": new RegExp(ename,"i")}, {"applicantInfo.eLastName":  new RegExp(ename,"i")}])
    }
    if(cname){
      respondObj = respondObj.or([{"applicantInfo.cFirstName": new RegExp(cname,"i")}, {"applicantInfo.cLastName":  new RegExp(cname,"i")}])

    }
    if(nation){
      respondObj = respondObj.where('applicantInfo.nation', new RegExp(nation,"i"))
    }
    respondObj.exec(
      function(err,result){
        if(err){
            next(err)
        }else{
          console.log(result)
          res.render('admin/search',{
            title: 'search result',
            session: req.session,
            respondObj: result
          })
        }
      }
    )
  }else{
    res.redirect('/adminMain/search/')
  }

  router.get('/calculate', function(req, res, next){
    adminUtils.userLoginChecker(req,res)
    applicantObj.find({}, function(err,result){
      if(err){
        console.log(err)
      }else{
        console.log(result)
        res.render('admin/listAll', {
          title: 'admin main page',
          session: req.session,
          json:result
        })

      }
    })
  })

})
router.get('/changedPw', function(req, res, next) {

  res.render('admin/changedPw', {
    title: 'settings',
    session: req.session
  })

})
router.post('/changePw', function(req, res, next) {
  adminUtils.userLoginChecker(req,res)
  var oldPassword = req.param('oldPassword', null)
  var newPassword = req.param('newPassword', null)
  var newHash = bcrypt.hashSync(newPassword, 10)
  var query = adminUserObj.findOne({"loginName":req.session.currentUser})
  query.exec(function(err,resultObj){
    if(err){
      console.log(err)
    }else if(resultObj){
      if(bcrypt.compareSync(oldPassword, resultObj.password)){
        resultObj.password = newHash
        resultObj.save(function(err){
          if(err){
            console.log(err)
          }else{
            req.session.destroy()
            res.redirect("/adminMain/changedPw/")
          }
        })
      }
    }else{

    }
  })

});
router.get('/options', function(req, res, next){
    optionsObj.find({},function(err,result){
      if(err){
        next(err)
      }else{
        console.log(result[0])
            res.render('admin/options', {
              title: "Options",
              session: req.session,
              o: result[0]
            })
      }
    })
});
router.post('/options',function(req,res,next){
    var mdAttention = req.param('mdAttention', null)
    var mdTerms = req.param('mdTerms', null)
    var acceptable = req.param('acceptable', null)
    var mdNotAccept = req.param('mdNotAccept', null)

    optionsObj.find({},function(err, results){
      if(err){
        console.log(err)
      }else if(results.length >= 1){
        console.log(results)
        var optResult = results[0]
        optResult.mdAttention = mdAttention
        optResult.mdTerms = mdTerms
        optResult.mdNotAccept = mdNotAccept
        optResult.acceptable = acceptable
        optResult.save(function(err,s){
          if(err){
            console.log(err)
          }else{
            console.log(s)
            res.redirect('/adminMain/options/')
          }
        })
      }else{
        var insertOptObj = new optionsObj({
          primaryKey: optionsObj.optionsPrimaryKey,
          mdAttention: mdAttention,
          mdTerms: mdTerms,
          acceptable: acceptable,
          mdNotAccept: mdNotAccept})
          insertOptObj.save(function(err){
            if(err){
              console.log(err)
            }else{
              console.log("Done!")
              res.redirect("/adminMain/options/")
            }
          })
      }
    })
})


router.post('/addUser', function(req, res, next) {
  console.log(req.session.currentUser)
  var username = req.param('username', null)
  var password = req.param('password', null)
  var email = req.param('email', null)

  var mdAttention = req.param('mdAttention', null)
  var mdTerms = req.param('mdTerms', null)
  var acceptable = req.param('acceptable', null)
  var mdNotAccept = req.param('mdNotAccept', null)

  if (!req.session.currentUser) {
    res.redirect("/admin/")
  }
  else if(username == "admin"){
    res.redirect("/adminMain/settings/")
  }else {
    adminUserObj.find({loginName: username},function(err, result) {
      if (err) {
        consle.log(err)
      } else {
        if (result.length == 0) {
          var hashPw = bcrypt.hashSync(password, 10)
          var insertObj = new adminUserObj({
            loginName: username,
            password: hashPw,
            email: email
          })
          insertObj.save(function(err, obj) {
            if (err) {
              console.log(err)
            } else {
                  res.send('<script>' +
                          'alert("Add scuess!");' +
                          ' window.location.href="/adminMain/settings/";'+
                          '</script>')




            }
          })
        } else {
          res.send('<script>' +
                  'alert("Add failed! Please make sure username not exist!");' +
                  ' window.location.href="/adminMain/settings/";'+
                  '</script>')


        }
      }
    })

  }
});
router.get('/removeUser', function(req, res, next){
  adminUtils.userLoginChecker(req,res)
  var uid = req.param('uid', null)
  adminUserObj.findOne({_id:uid}, function(err, result){
    if(err){
      next(err)
    }else if(result){
      if(result.loginName == "admin"){

        res.send("<script>" +
                "alert('admin Cannot been removed.');" +
                'window.location.href= "/adminMain/settings/";' +
                "</script>")
      }else{

          adminUserObj.findByIdAndRemove(uid, {} , function(err){
              if(err){
                console.log(err)
              }else{
                if(req.session.currentUser = result.loginName){
                  req.session.destroy()
                }
                res.send("<script>" +
                        "alert('remove scuess!');" +
                        'window.location.href= "/adminMain/settings/";' +
                        "</script>")
              }

          })
      }
    }else{


          res.send("<script>" +
                  "alert('Invaild uid.');" +
                  'window.location.href= "/adminMain/";' +
                  "</script>")
    }
  })

})
router.get ('/resetPw', function(req,res,next){

  res.render('admin/resetPw', {
    title: "Reset Password",
    session: req.session
  })
})
router.post('/resetPw', function(req, res, next) {

    //adminUtils.userLoginChecker(req,res)
  var mailgunUtils = require('./modules/mailgunUtils.js')
  var api_key = mailgunUtils.mailgunApi
  var domain = mailgunUtils.domain
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain})
  var username = req.param('username', null)
  var email = req.param('email', null)
  var newPassword = require("randomstring").generate(12)
  var redirectUrl = req.param('redirectUrl',null)

  var hostDomain = req.get('host')

  var newHash = bcrypt.hashSync(newPassword, 10)
  var query = adminUserObj.findOne({"loginName": username, email:email})
  query.exec(function(err,resultObj){
    if(err){
      console.log(err)
    }else if(resultObj && username && email){
        resultObj.password = newHash
        resultObj.save(function(err){
          if(err){
            console.log(err)
          }else{
            var sentData = {
              from: 'Auto System <noreply@' + domain +  '>',
              to: email,
              subject: 'Your account password on ' + hostDomain + ' has been reset.',
              text: ' Hello, ' + username +
                    '\n You new password is ' + newPassword +
                    '\n plase go to ' + hostDomain + "/admin/ to login and change to your new password." +
                    '\n Auto System'
            }
              mailgun.messages().send(sentData, function (error, body) {
                console.log(body);
              if(!redirectUrl){
                var rUrl = "/adminMain/"
              }else{
                var rUrl = redirectUrl
              }
              res.send('<script>' +
                        'alert("'+ body.message + '");' +
                        'window.location.href = "'+ rUrl +'";' +
                        "</script>")
              })
          }
        })

    }else{
      if(redirectUrl){
        var tUrl = redirectUrl
      }else{
        var tUrl = "/adminMain/resetPw/"
      }
      res.send("<script>" +
              "alert('Reset failed! Check the information is corrent.');" +
              'window.location.href= "'+ tUrl + '";' +
              "</script>")
    }
  })

})


router.get('/export', function(req, res, next){
  adminUtils.userLoginChecker(req,res)
  applicantObj.find({}, function(err,result){
    if(err){
      console.log(err)
    }else{
      console.log(result)
      res.xls('data.xlsx', result)

    }
  })
})
module.exports = router;
