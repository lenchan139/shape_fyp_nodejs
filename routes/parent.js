var express = require('express');
var router = express.Router();
var aws = require('aws-sdk')
var multer  = require('multer')
var multerS3 = require('multer-s3')
var s3 = new aws.S3({ /* ... */ })
var parentUtils = require('./modules/ParentUtils.js')
var pwUtils = require('./modules/pwUtils.js')
var bcrypt = require('bcrypt');
var fileMessages = parentUtils.parentFileUploadMessage
var fileUploader = multer({ dest: 'fileUploads/' });

var upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: 'shape-fyp',
    acl: 'public-read',
    metadata: function (req, file, cb) {
      cb(null, {fieldName: file.fieldname});
    },
    key: function (req, file, cb) {
      cb(null, Date.now().toString())
    }
  })
})
// init mongodboose
var mongoose = require('mongoose');
var adminUtils = require('./modules/AdminUtils.js')
mongoose.connect('mongodb://generealUser:123456@ds247347.mlab.com:47347/shape_fyp');
console.log(parentUtils.getBaseApplicantObject)
var applicantObj = parentUtils.getBaseApplicantObject(mongoose);
var applicantLoginObj = parentUtils.getBaseApplicantLoginObject(mongoose);
var optionsObj = adminUtils.getBaseOptionsObj(mongoose);
router.get('/', function(req, res, next) {
  optionsObj.find({}, function(err, results){
    console.log(results)
    if(err){
      console.log(err)
    }else{
    var showdown = require('showdown')
    var converter = new showdown.Converter()
    var md ={}
    if(results[0]){
      md.mdAttention = results[0].mdAttention
      md.mdTerms = results[0].mdTerms
      md.acceptable = results[0].acceptable
      md.mdNotAccept = results[0].mdNotAccept
    }
    console.log(md)
    if(md.acceptable){

      res.render('parent/parent', {
          title: 'parent',
          md : md
        });
    }else{
      var mdNotaccept = converter.makeHtml(results[0].mdNotAccept)
      console.log(mdNotaccept)
      res.render('parent/parentNot', {
          title: 'parent',
          mdNotaccept : mdNotaccept
        });
    }
  }
})

});
router.post('/', function(req, res, next) {
  var insertObj = parentUtils.getFormattedApplicantObjectFromRequestPaster(req,applicantObj)
  console.log(req.param('parent',null))
  insertObj.save(function(err, obj) {
    if (err) {
      console.log(err)
    } else {
      res.redirect('/parent/fileUpload/?id='+obj._id)
    }
  })
})
router.get('/familyInfoloadModule', function(req, res, next){
  var fNumber = req.param('fNumber', 1)
  res.render('parent/familyInfoloadModule',{fNumber:fNumber})
})

router.post('/fileUpload',upload.single('fileUpload' ), function(req, res, next) {
  var file = req.file
  var comment = req.param('comment',null)
  var id = req.param('id',null)
  var step = parseInt(req.param('step', 0))
  var skip = req.param('skip', false)
  var query = applicantObj.findOne({"_id":id})
  console.log(file)
  if(skip){
    res.redirect('/parent/fileUpload/?id=' + id
            + '&msg=' + fileMessages.failed.key
            + '&step=' + step+1)
  }
  if(!file || !comment || !id ){
    res.redirect('/parent/fileUpload/?id=' + id
            + '&msg=' + fileMessages.failed.key
            + '&step=' + step)
  }else if(file.mimetype != "image/jpeg" && file.mimetype != "image/png"){
    res.redirect('/parent/fileUpload/?id=' + id
        + '&msg=' + fileMessages.invaildFormat.key
        + '&step=' + step)
  }else{
  query.exec(function(err, result){
    if(err){
      console.log(err)
    }else if(result){
      console.log(result)

      var pushObj = {
        filePath : file.location,
        comment : comment
      }
      applicantObj.update(
        {"_id":id},
        { $push: {"fileUploads": pushObj}},
        function(err,rObj){
          console.log("done")
          if(err){
            console.log(err)
          }else{
          res.redirect(303, '/parent/fileUpload/?id=' + id + "&msg=" + fileMessages.success.key
        +"&step=" + step)
          }
        })
    }else{

    }
  })
}
});

router.get('/fileUpload', function(req, res, next) {
  var id = req.param('id', null)
  var msgKey = req.param('msg',null)
  var msg = "";
  var parentScoreMsg = parentUtils.parentScoreUpMesg;
  var step = parseInt(req.param('step', 0))
  var stepMsg;
  var skip = req.param('skip', false)
  if(id == null){
    res.redirect('/')
  }
  if(msgKey == fileMessages.success.key){
    msg = fileMessages.success.message
    step = step + 1
  }else if(msgKey == fileMessages.failed.key){
    msg = fileMessages.failed.message
  }else if(msgKey == fileMessages.invaildFormat.key){
    msg = fileMessages.invaildFormat.message
  }


  if(step<= parentScoreMsg.length-1){
    stepMsg = parentScoreMsg[step]
  }
  res.render('parent/fileUpload', {
    title: 'file upload',
    msg: msg,
    step:step,
    stepMsg: stepMsg,
    id:id
  });
});


router.get('/pwSetting', function(req, res, next) {
  var id = req.param('id', null)
  var msgKey = req.param('msg',null)
  var msg = "";
  var hostDomain = req.get('host')
  if(id == null){
    res.redirect('/')
  }

  res.render('parent/pwSetting', {
    title: 'Setup password ',
    msg: msg,
    hostDomain: hostDomain,
    uid: id
  });
})

router.post('/pwSetting', function(req, res, next) {
  var id = req.param('id', null)
  var password = req.param('password', null)
  var msg = "";
  var hostDomain = req.get('host')
  if(id == null){
    res.redirect('/')
  }
  bcrypt.hash(password,pwUtils.saltRounds,function(err,hash){
    if(err){
      res.redner('err',{
        error: err
      })
    }else{

      var query = applicantLoginObj.findOne({"uid":id})
      query.exec(function(err, r){
        if(err){
          next(err)
        }else if(r){
          res.render('error', {
            message: "The password of this user has been set."
          })
        }
        else{
          console.log(r)
          var insertObj = new applicantLoginObj({
            uid : id ,
            password : hash
          })
          insertObj.save(function(err, obj) {
            if(err){
              next(err)
            }else{
              res.redirect('/parent/done/')
              /*res.render('parent/pwSetting', {
                title: 'Setup password ',
                msg: msg,
                hostDomain: hostDomain,
                uid: id,
                password:hash
              });*/
            }
          })
        }
      })
    }
  })

})
router.get('/done', function(req, res, next) {
  res.render('parent/done', {
    title: 'done'
  });
});

router.post('/loadFromExt', function(req, res,next){
  var jsonString = req.param('jsonParent',null)
  if(jsonString){
    var json = JSON.parse(jsonString)
    console.log(json)
    res.render('parent/loadDetail', {
      json:json,
      parent:json.familyInfo,
      file: json.fileUploads
    })
  }else{

  }
})

router.post('/updateFromExt', function(req, res,next){
  var jsonString = req.param('json',null)
  if(jsonString){
    var json = JSON.parse(jsonString)
    console.log(json)
    var insertObj = parentUtils.getFormattedApplicantObjectFromExistObjects(json,applicantObj)
    insertObj.save(function(err, obj) {
      if (err) {
        console.log(err)
      } else {
        res.redirect('/parent/pwSetting/?id='+obj._id)
      }
    })
  }else{

  }
})

module.exports = router;
