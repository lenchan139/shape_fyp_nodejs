var express = require('express');
var router = express.Router();
// init mongodboose
var mongoose = require('mongoose');
var adminUtils = require('./modules/AdminUtils.js')
mongoose.connect('mongodb://generealUser:123456@ds247347.mlab.com:47347/shape_fyp');
var AdminUserObj = adminUtils.getBaseAdminUserObj(mongoose)
var optionsObj = adminUtils.getBaseOptionsObj(mongoose)
var bcrypt = require('bcrypt')
/* GET login page page. */
router.get('/', function(req, res, next) {
  var msgKey = req.param('msg')
  var msg = ""
  console.log(req.session.currentUser)
  if (req.session.currentUser) {
    res.redirect("/adminMain")
  } else {
    AdminUserObj.find(function(err, result) {
      if (err) {
        consle.log(err)
      } else {
        console.log(result)
        if (!result) {

        } else if (result.length > 0) {
          if (adminUtils.adminMsg.firstRegisterDone.key == msgKey) {
            msg = adminUtils.adminMsg.firstRegisterDone.message
          } else if (adminUtils.adminMsg.pwOrUsernameIncorrent.key == msgKey) {
            msg = adminUtils.adminMsg.pwOrUsernameIncorrent.message
          }
          res.render('admin/login', {
            title: 'not login yet',
            msg: msg
          });
        } else {
          res.redirect("/admin/firstRegister/")
        }
      }
    });

  }

});
router.post('/', function(req, res, next) {
  var username = req.param("username", null)
  var password = req.param("password", null)

  var query =  AdminUserObj.findOne({'loginName': username})
    query.exec(function(err, results) {
      if (err) {
        console.log(err);
      } else {
        console.log(results)
        if (results) {
          if (results.loginName == username && bcrypt.compareSync(password, results.password)) {
            console.log(results);
            req.session.currentUser = results.loginName
            res.redirect('/adminMain/')
          } else {
            res.redirect('/admin/?msg=' + adminUtils.adminMsg.pwOrUsernameIncorrent.key)
          }
        }else{

            res.redirect('/admin/?msg=' + adminUtils.adminMsg.pwOrUsernameIncorrent.key)
        }
      }
    });

});

router.get('/firstRegister', function(req, res, next) {
  console.log(req.session.currentUser)
  if (req.session.currentUser) {
    res.redirect("/admin/main")
  } else {
    AdminUserObj.find(function(err, result) {
      if (err) {
        consle.log(err)
      } else {
        if (result.length == 0) {
          res.render('admin/firstRegister', {
            title: 'first register!'
          });
        } else {
          res.redirect('/admin/')
        }
      }
    })

  }
});
router.post('/firstRegister', function(req, res, next) {
  console.log(req.session.currentUser)
  var username = "admin"
  var password = req.param('password', null)
  var email = req.param('email', null)

  var mdAttention = req.param('mdAttention', null)
  var mdTerms = req.param('mdTerms', null)
  var acceptable = req.param('acceptable', null)
  var mdNotAccept = req.param('mdNotAccept', null)

  if (req.session.currentUser) {
    res.redirect("/admin/main")
  } else {
    AdminUserObj.find(function(err, result) {
      if (err) {
        consle.log(err)
      } else {
        if (result.length == 0) {
          var hashPw = bcrypt.hashSync(password, 10)
          var insertObj = new AdminUserObj({
            loginName: username,
            password: hashPw,
            email: email
          })
          insertObj.save(function(err, obj) {
            if (err) {
              console.log(err)
            } else {
              optionsObj.find(function(err, oResult){
                if(err){
                    next(err)
                }else if(oResult.length <= 0){

                    var insertOptObj = new optionsObj({
                      primaryKey: optionsObj.optionsPrimaryKey,
                      mdAttention: mdAttention,
                      mdTerms: mdTerms,
                      acceptable: acceptable,
                      mdNotAccept: mdNotAccept})
                      insertOptObj.save(function(err){
                        if(err){
                          console.log(err)
                        }else{
                          console.log("Done!")
                            res.redirect('/admin/?msg=' + adminUtils.adminMsg.firstRegisterDone.key)
                        }
                      })
                }else{

                      optionsObj.find({},function(err, results){
                        if(err){
                          console.log(err)
                        }else if(results.length >= 1){
                          console.log(results)
                          var optResult = results[0]
                          optResult.mdAttention = mdAttention
                          optResult.mdTerms = mdTerms
                          optResult.mdNotAccept = mdNotAccept
                          optResult.acceptable = acceptable
                          optResult.save(function(err,s){
                            if(err){
                              console.log(err)
                            }else{
                              console.log("Done!")
                              res.redirect('/admin/?msg=' + adminUtils.adminMsg.firstRegisterDone.key)

                            }
                          })

                        }
                      })


              }
            })
        }
      })
    }else {
      res.redirect('/admin/')
    }

  }
});
}
})
router.get('/logout', function(req, res, next) {
  req.session.destroy()
  res.render('admin/logout',{title:"Logout"})
});


module.exports = router;
