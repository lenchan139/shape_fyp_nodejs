var express = require('express');
var router = express.Router();
var fs = require('fs')
/* GET home page. */
router.get('/:id', function(req, res, next) {
  var id = req.params.id
  console.log(id)
  fs.readFile('./fileUploads/'+id, function(err, data) {
    if(err){
      console.log(err)
    }else{
      res.end(data)
    }
  })

});

module.exports = router;
