exports.getBaseApplicantObject = function(mongoose) {
   return mongoose.models.applicantObj || mongoose.model('applicantObj', new mongoose.Schema({
    applicantInfo: {
      eFirstName: String,
      eLastName: String,
      cFirstName: String,
      cLastName: String,
      spokenLanguage: String,
      gender: String,
      age: Number,
      birthday: Date,
      nation: String,
      placeOfBirth: String,
      religion: String,
      religiousName: String,
      embraceDate: Date,
      religiousDenomination: String,
      email: String,
      address: String,
      telephone: Number
    },
    familyInfo: [Array],
    fileUploads: [Array]
  }));
}
exports.getBaseApplicantLoginObject = function(mongoose) {
   return mongoose.models.applicantLoginObj || mongoose.model('applicantLoginObj', new mongoose.Schema({
     uid : String,
     password : String
   }))
 }

exports.getFormattedApplicantObjectFromExistObjects = function(e, applicantObj) {
  return new applicantObj({
    applicantInfo: e.applicantInfo,
    familyInfo: e.familyInfo,
    fileUploads: e.fileUploads
  })
}
exports.getFormattedApplicantObjectFromRequestPaster = function(req, applicantObj) {
  return new applicantObj({
    applicantInfo: {
      eFirstName: req.param('eFirstName', null),
      eLastName: req.param('eLastName', null),
      cFirstName: req.param('cFirstName', null),
      cLastName: req.param('cLastName', null),
      spokenLanguage: req.param('spokenLang', null),
      gender: req.param('gender', null),
      age: req.param('age', null),
      birthday: req.param('birthday', null),
      nation: req.param('nation', null),
      placeOfBirth: req.param('placeOfBirth', null),
      religion: req.param('religion', null),
      religiousName: req.param('religiousName', null),
      embraceDate: req.param('embraceDate', null),
      religiousDenomination: req.param('religiousDenomination', null),
      email: req.param('email', null),
      address: req.param('address', null),
      telephone: req.param('telephone', null)
    },
    familyInfo: req.param('parent',[])

  })
}

exports.parentFileUploadMessage = {
  success: {
    key: "success",
    message: "Upload successfully!"
  },
  failed: {
    key: "failed",
    message: "Upload Failed! try agin!"
  },
  invaildFormat: {
    key: "invaildFormat",
    message: "The format of file is invaild, please upload jpg or png."
  }
}
exports.parentScoreUpMesg = [
  "Parent(s) teaching or working full-time in the kindergarten or secondary section if it is of the same address as the primary school.",
  "Sibling(s) studying in the secondary section if it is of the same address as the primary school.",
  "Parent(s) being a school manager of the primary school.",
  "Parent(s) or sibling(s) being a graduate of the primary school.",
  "First-born child (the eldest child in the family irrespective of sex).",
  "Same religious affiliation as the sponsoring body which operates the primary school.",
  "Parent(s) being a member of the same organisation which sponsors the operation of the primary school.",
  "Applicant child of the right age (5 years 8 months to 7 years old)"
]
