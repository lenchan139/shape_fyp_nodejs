exports.adminMsg = {
  pwOrUsernameIncorrent : {
    key : "pwOrUsernameIncorrent",
    message : "The username or password incorrent."
  },
  firstRegisterDone : {
    key : "firstRegisterDone",
    message : "Admin Account has been created! please login now."
  }
}
exports.getBaseAdminUserObj = function(mongoose){
  return mongoose.models.AdminUserObj || mongoose.model('AdminUserObj', {
    loginName: String,
    password: String,
    email: String
  });
}
exports.userLoginChecker = function(req,res){
  if (!req.session.currentUser) {
    res.redirect("/admin/")
  }
}

exports.getBaseOptionsObj = function(mongoose){
  return mongoose.models.OptionsObj || mongoose.model('OptionsObj', {
    primaryKey : String,
    mdAttention : String,
    mdTerms : String,
    mdNotAccept : String,
    acceptable: Boolean
  })
}

exports.optionsPrimaryKey = "asu1i12sh1nugvsd2093jdijs"
