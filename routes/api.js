var express = require('express');
var router = express.Router();
var aws = require('aws-sdk')
var multer  = require('multer')
var multerS3 = require('multer-s3')
var parentUtils = require('./modules/ParentUtils.js')
var pwUtils = require('./modules/pwUtils.js')
var bcrypt = require('bcrypt');

// init mongodboose
var mongoose = require('mongoose');
var adminUtils = require('./modules/AdminUtils.js')
mongoose.connect('mongodb://generealUser:123456@ds247347.mlab.com:47347/shape_fyp');
console.log(parentUtils.getBaseApplicantObject)
var applicantObj = parentUtils.getBaseApplicantObject(mongoose);
var applicantLoginObj = parentUtils.getBaseApplicantLoginObject(mongoose);
var optionsObj = adminUtils.getBaseOptionsObj(mongoose);

//route function start
router.get('/loadApplicantrInfo', function(req, res, next) {
  var uid = req.param("uid", null)
  var password = req.param("password",null)
  var result = {}
  console.log(uid)
  if(uid && password){
    applicantLoginObj.findOne({uid:uid},function(err,obj){

      if(err){
        next(err)
      }else if(!obj){
        result.isVaild = false
        res.send(result)
      }else{
        bcrypt.compare(password, obj.password, function(err, isSame){
          if(isSame){
            result.isVaild = true
            applicantObj.findOne({"_id":uid},function(err,applicant){
              if(err){
                next(err)
              }else{
                result.applicant = applicant
                res.send(result)
              }
            })
          }else{
            result.isVaild = false
            res.send(result)
          }
        })
      }
    })
  }else{

      res.send(result)
  }

});

module.exports = router;
