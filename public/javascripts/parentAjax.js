var BLOCK = "block"
var NONE = "NONE"
function showHome(){
  document.getElementById('mdAttention').style.display = BLOCK
  document.getElementById('loadFromExt').style.display = NONE
  document.getElementById('appForm').style.display = NONE
  //document.getElementById('mdAttention').style.display = NONE

}
function showNewApp(){
  document.getElementById('mdAttention').style.display = NONE
  document.getElementById('loadFromExt').style.display = NONE
  document.getElementById('appForm').style.display = BLOCK
  //document.getElementById('mdAttention').style.display = NONE

}
function showOldApp(){
  document.getElementById('mdAttention').style.display = NONE
  document.getElementById('loadFromExt').style.display = BLOCK
  document.getElementById('appForm').style.display = NONE
  //document.getElementById('mdAttention').style.display = NONE

}
document.addEventListener("DOMContentLoaded", function(){
  // Handler when the DOM is fully loaded
  //toggleLoadExt()
  showHome()
  showOneBlock('nameBlock')
});
function hideAllBlock(){
  document.getElementById('nameBlock').style.display = NONE
  document.getElementById('languageBlock').style.display = NONE
  document.getElementById('basicInfoBlock').style.display = NONE
  document.getElementById('askNationBlock').style.display = NONE
  document.getElementById('nationBlock').style.display = NONE
  document.getElementById('askReligionBlock').style.display = NONE
  document.getElementById('ansReligionBLock').style.display = NONE
  document.getElementById('contactBlock').style.display = NONE
  document.getElementById('familyBlock').style.display = NONE
  document.getElementById('endBlock').style.display = NONE
}
function showOneBlock(blockId){
  hideAllBlock()
  document.getElementById(blockId).style.display = BLOCK
}

function isNameVaild(){
  var isVaild = document.getElementById('eFirstName').value && document.getElementById('eLastName').value && document.getElementById('cFirstName').value && document.getElementById('cLastName').value
  if(isVaild){
    showOneBlock('languageBlock')
  }else {
    alert('Please corrent the name!')
  }
}
function isLangVaild(){
  if(document.getElementById('spokenLang').value){
    showOneBlock('basicInfoBlock')
  }else{
    alert('Please select!')
  }
}
function isBasicInfoVaild(){
  var vaild = document.getElementById('birthday').value && document.getElementById('age').value;
  if(vaild){
    showOneBlock('askNationBlock')
  }else{
    alert('Please fill all or check your information')
  }
}
function nationVaild(){
  var vaild = document.getElementById('nation').value && document.getElementById('placeOfBirth').value
  if(vaild){
    showOneBlock('askReligionBlock')
  }else{
    alert('Please fill it completely!')
  }
}
function checkReligion(){
  var vaild = document.getElementById('religiousDenomination').value && document.getElementById('embraceDate').value && document.getElementById('religiousName').value && document.getElementById('religion').value
  if(vaild){
    showOneBlock('contactBlock')
  }else{
      alert('Please fill it completely!')
  }
}
function checkContact(){
  var vaild = document.getElementById('telephone').value && document.getElementById('address').value && document.getElementById('email').value
  if(vaild){
    showOneBlock('familyBlock')
  }else{
      alert('Please fill it completely!')
  }
}
function fillNationHK(){
  document.getElementById('placeOfBirth').value = "Hong Kong"
  document.getElementById('nation').value = "China"
  upgradeInputDiv(document.getElementById('placeOfBirth'))
  upgradeInputDiv(document.getElementById('nation'))
}

function fillReligionHK(){
  document.getElementById('religion').value = "N/A"
  document.getElementById('religiousName').value = "N/A"
  document.getElementById('embraceDate').value = "0001-01-01"
  document.getElementById('religiousDenomination').value = "N/A"
  upgradeInputDiv(document.getElementById('religion'))
  upgradeInputDiv(document.getElementById('religiousName'))
  upgradeInputDiv(document.getElementById('embraceDate'))
  upgradeInputDiv(document.getElementById('religiousDenomination'))
}
