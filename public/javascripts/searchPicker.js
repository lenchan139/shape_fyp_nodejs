function hideAllSearchAttr(){
  upgradeInputDiv(document.getElementById('sEName'))
  upgradeInputDiv(document.getElementById('sCName'))
  upgradeInputDiv(document.getElementById('sNation'))
  upgradeInputDiv(document.getElementById('sBirth'))
  upgradeInputDiv(document.getElementById('sRelogion'))
  upgradeInputDiv(document.getElementById('sRelogiodeno'))
  upgradeInputDiv(document.getElementById('sAddress'))
  upgradeInputDiv(document.getElementById('sParentOcc'))
  document.getElementById('searchForm').reset()

}

function upgradeInputDiv(tDiv){
  tDiv.classList.remove('is-invalid')
  tDiv.classList.remove('is-upgraded')
  tDiv.style.display = "none"
}
function showOneSearchAttr(t){
  document.getElementById(t).style.display = "block"
}
function onSelectChange(obj){
  let value = obj.value
  hideAllSearchAttr()
  showOneSearchAttr(value)
}
document.addEventListener("DOMContentLoaded", function(){
  hideAllSearchAttr()
  showOneSearchAttr('sEName')
});
