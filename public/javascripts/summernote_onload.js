var loader = function(note,sync){
    $(note).summernote({

    height: 300
  });

  $(note).summernote('editor.pasteHTML', $(sync).val())
  $(note).summernote({
    onChange: function(contents, $editable) {
      console.log('onChange:', contents, $editable);
    }
  }).on('summernote.change', function(customEvent, contents, $editable) {
    console.log('onChange:', contents, $editable);
    // you can get code like this at v0.6.6
    console.log($(note).summernote('code'));
    $(sync).val($(note).summernote('code'))
  });
}
$(document).ready(function() {
  loader('#smNotAccept','#mdNotAccept')
  loader('#smAttention','#mdAttention')
  loader('#smTerms','#mdTerms')
})
