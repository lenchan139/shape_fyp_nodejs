var familyMemberCount = 1;
var ajaxGet = function (url, callback) {
    var callback = (typeof callback == 'function' ? callback : false), xhr = null;
    try {
      xhr = new XMLHttpRequest();
    } catch (e) {
      try {
        xhr = new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e) {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
      }
    }
    if (!xhr)
           return null;
    xhr.open("GET", url,true);
    xhr.onreadystatechange=function() {
      if (xhr.readyState==4 && callback) {
        callback(xhr.responseText)
      }
    }
    xhr.send(null);
    return xhr;
}


function toggleLoadExt() {
    var x = document.getElementById("mainContentOfLoadExt");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
function toggleCompleteForm(){
  var x = document.getElementById("completeForm");
  if (x.style.display === "none") {
      x.style.display = "block";
  } else {
      x.style.display = "none";
  }
}
document.addEventListener("DOMContentLoaded", function(){
  // Handler when the DOM is fully loaded
  //toggleLoadExt()

  var eFirstName = document.getElementById("eFirstName")
  //eFirstName.required= false
});


function loadExtFunc(){
  var host = document.getElementById("extHostDomain");
  var uid = document.getElementById("extUid");
  var password = document.getElementById("extPassword");
  if(host && uid && password){
    console.log(host.value + "|" + uid.value + "|" + password.value)
    var url = host.value + "/api/loadApplicantrInfo/?uid=" + uid.value + "&password=" + password.value
    ajaxGet(url,function(res){
      var json = {}
      try {
        json = JSON.parse(res)
        if(!json){
          console.log('something was wrong....')
        }else if(!json.isVaild){
          console.log('ID and password is mismatch!')
        }else{
          var applicant = json.applicant
          loadExtToForm(applicant)
        }
      } catch (e) {
        console.log('Not vaild host or api service not found.\n' + e)

      }
    })
  }
}

function upgradeInputDiv(tDiv){
  tDiv.classList.remove('is-invalid')
  tDiv.classList.remove('is-upgraded')
  tDiv.classList.add('is-dirty')
}
function loadExtToForm(applicant){

  if(applicant){
    console.log(applicant)
    var info = applicant.applicantInfo
    var jsonParent = document.getElementById("jsonParent")
    jsonParent.value = JSON.stringify(applicant)
    var f = document.getElementById("loadExtForm")
    f.action = "/parent/loadFromExt/"
    var button = document.getElementById('loadExtButton')
    button.removeAttribute('disabled')
  }else{

  }
}

function addFamilyGroup(){

  var c = document.createElement('p');
  var host = window.location.hostname;
  console.log(host)
  var url = "/parent/familyInfoloadModule/?fNumber=" + familyMemberCount
  ajaxGet(url,function(res){
    var json = {}
    try {
      if(!res){
        console.log('something was wrong....')
      }else{
        familyMemberCount = familyMemberCount + 1
      c.innerHTML = "<h5>Family Member " + familyMemberCount + "</h5<" + res
      document.getElementById('familyInfo').appendChild(c);
      componentHandler.upgradeDom();
      }
    } catch (e) {
      console.log('Not vaild host or api service not found.\n' + e)

    }
  })
}


function loadExtToForm1(applicant){

    console.log(applicant)
  if(applicant){
    var info = applicant.applicantInfo

    var jsonParent = document.getElementById("jsonParent")
    jsonParent.value = JSON.stringify(applicant)

    //1
    var eFirstName = document.getElementById("eFirstName")
    eFirstName.required = false
    eFirstName.value = info.eFirstName
    if(eFirstName.value){
      upgradeInputDiv(document.getElementById("eFirstNameDiv"))
    }
    //2
    var eLastName = document.getElementById("eLastName")
    eLastName.required = false
    eLastName.value = info.eLastName
    if(eLastName.value){
      upgradeInputDiv(document.getElementById("eLastNameDiv"))
    }
    //3
    var cFirstName = document.getElementById("cFirstName")
    cFirstName.required = false
    cFirstName.value = info.cFirstName
    if(cFirstName.value){
      upgradeInputDiv(document.getElementById("cFirstNameDiv"))
    }
    //4
    var cLastName = document.getElementById("cLastName")
    cLastName.required = false
    cLastName.value = info.cLastName
    if(cLastName.value){
      upgradeInputDiv(document.getElementById("cLastNameDiv"))
    }
    var spokenLang = document.getElementById("spokenLang")
    spokenLang.value = info.spokenLanguage

    var genderMale = document.getElementById("male")
    var genderFemale = document.getElementById("female")
    if(info.gender == "male"){
      genderMale.checked = true
    }else if(info.gender == "female"){
      genderFemale.checked = true
    }
    var age = document.getElementById("age")
    age.required = false
    age.value = info.age
    if(age.value){
      upgradeInputDiv(document.getElementById("ageDiv"))
    }
    var birthday = document.getElementById("birthday")
    var bDate = info.birthday.substring(0,info.birthday.indexOf("T0"))
    birthday.required = false
    birthday.value = bDate
    if(birthday.value){
      upgradeInputDiv(document.getElementById("birthdayDiv"))
    }

    var placeOfBirth = document.getElementById("placeOfBirth")
    placeOfBirth.required = false
    placeOfBirth.value = info.placeOfBirth
    if(placeOfBirth.value){
      upgradeInputDiv(document.getElementById("placeOfBirthDiv"))
    }

    var nation = document.getElementById("nation")
    nation.required = false
    nation.value = info.nation
    upgradeInputDiv(document.getElementById("nationDiv"))

    var religion = document.getElementById("religion")
    religion.required = false
    religion.value = info.religion
    if(religion.value){
      upgradeInputDiv(document.getElementById("religionDiv"))
    }

    var religiousName = document.getElementById("religiousName")
    religiousName.required = false
    religiousName.value = info.religiousName
    upgradeInputDiv(document.getElementById("religiousNameDiv"))

    var embraceDate = document.getElementById("embraceDate")
    embraceDate.required = false
    var eDate = info.embraceDate.substring(0,info.embraceDate.indexOf("T0"))
    embraceDate.value = eDate
    if(embraceDate.value){
      upgradeInputDiv(document.getElementById("embraceDateDiv"))
    }
    var religiousDenomination = document.getElementById("religiousDenomination")
    religiousDenomination.required = false
    religiousDenomination.value = info.religiousDenomination
    if(religiousDenomination.value){
      upgradeInputDiv(document.getElementById("religiousDenominationDiv"))
    }
    var email = document.getElementById("email")
    email.required = false
    email.value = info.email
    if(email.value){
      upgradeInputDiv(document.getElementById("emailDiv"))
    }
    var address = document.getElementById("address")
    address.required = false
    address.value = info.address
    if(address.value){
      upgradeInputDiv(document.getElementById("addressDiv"))
    }
    var telephone = document.getElementById("telephone")
    telephone.required = false
    telephone.value = info.telephone
    if(telephone.value){
      upgradeInputDiv(document.getElementById("telephoneDiv"))
    }



    //start with family member
    var family = applicant.familyInfo
    for(var i = 0; i < family.length;i++){
      var x = i + 1
      finfo = family[i]
      if(finfo.eFirstName && finfo.eLastName && finfo.cFirstName && finfo.cLastName && finfo.age){

        var eFirstName = document.getElementById("parEFirstname" + x)
        eFirstName.required = false
        eFirstName.value = finfo.eFirstName
        if(eFirstName.value){
          upgradeInputDiv(document.getElementById("parEFirstname" + "Div" + x))
        }
        var parELastname = document.getElementById("parELastname" + x)
        parELastname.required = false
        parELastname.value = finfo.eLastName
        if(parELastname.value){
          upgradeInputDiv(document.getElementById("parELastname" + "Div" + x))
        }
        var parCFirstname = document.getElementById("parCFirstname" + x)
        parCFirstname.required = false
        parCFirstname.value = finfo.cFirstName
        if(parCFirstname.value){
          upgradeInputDiv(document.getElementById("parCFirstname" + "Div" + x))
        }
        var parCLastname = document.getElementById("parCLastname" + x)
        parCLastname.required = false
        parCLastname.value = finfo.cLastName
        if(parCLastname.value){
          upgradeInputDiv(document.getElementById("parCLastname" + "Div" + x))
        }
        var parRelationship = document.getElementById("parRelationship" + x)
        parRelationship.required = false
        parRelationship.value = finfo.relationship
        if(parRelationship.value){
          upgradeInputDiv(document.getElementById("parRelationship" + "Div" + x))
        }
        var parAge = document.getElementById("parAge" + x)
        parAge.required = false
        parAge.value = finfo.age
        if(parAge.value){
          upgradeInputDiv(document.getElementById("parAge" + "Div" + x))
        }
        var parHKID = document.getElementById("parHKID" + x)
        parHKID.required = false
        parHKID.value = finfo.hkid
        if(parHKID.value){
          upgradeInputDiv(document.getElementById("parHKID" + "Div" + x))
        }
        var parAcademicQualify = document.getElementById("parAcademicQualify" + x)
        parAcademicQualify.required = false
        parAcademicQualify.value = finfo.academicQualify
        if(parAcademicQualify.value){
          upgradeInputDiv(document.getElementById("parAcademicQualify" + "Div" + x))
        }
        var parOccupation = document.getElementById("parOccupation" + x)
        parOccupation.required = false
        parOccupation.value = finfo.occupation
        if(parOccupation.value){
          upgradeInputDiv(document.getElementById("parOccupation" + "Div" + x))
        }
        var parContactNum = document.getElementById("parContactNum" + x)
        parContactNum.required = false
        parContactNum.value = finfo.contactNum
        if(parContactNum.value){
          upgradeInputDiv(document.getElementById("parContactNum" + "Div" + x))
        }
      }
    }
  }else{

  }
}
